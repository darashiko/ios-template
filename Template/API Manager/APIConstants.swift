//
//  APIConstants.swift
//  HotWorx
//
//  Created by Akber Sayni on 10/07/2018.
//  Copyright © 2018 M.Usman Bin Rehan. All rights reserved.
//

import UIKit

enum WebRoute: String {
    case Login = "login"
    case Logout = "logout"
}
