//
//  UIFont+Extension.swift
//  Auditix
//
//  Created by M.Usman Bin Rehan on 1/3/18.
//  Copyright © 2018 M.Usman Bin Rehan. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    func sizeOfString(text: NSString, constrainedToWidth width: Double) -> CGSize {
        return text.boundingRect(with: CGSize(width: width, height: .greatestFiniteMagnitude),
                                 options: .usesLineFragmentOrigin,
                                 attributes: [NSAttributedStringKey.font: self],
                                 context: nil).size
    }
}
